$fn=60;

he= 36.6+4;

module copy_mirror(axis){
    children();
    mirror(axis) children();
}

module roller(dia=24,extend=false, nowheel=false){
    halfw=8/2+0.2;
    dia = extend?dia+1:dia;
    if(!nowheel){
        difference(){
            copy_mirror([0,0,1]) union(){
                cylinder(d=dia,h=halfw);
                translate([0,0,halfw]) cylinder(d1=dia,d2=dia+4,h=1.5);
                translate([0,0,halfw+1.5]) cylinder(d=dia+4,h=extend?1:0.5);
            }
            translate([0,0,-5/2]) cylinder(d=16,h=15);
            translate([0,0,-10]) cylinder(d=16-(extend?2:1),h=15);
        }
    }
    difference(){
        union(){
            cylinder(d=5,h=5,center=true);
            translate([0,0,-4-2.5])cylinder(d1=13, d2=5+2,h=4);
            
            translate([0,0,2.5+0.3])cylinder(d=8,h=2);
        }
        translate([0,0,-15+2.5])cylinder(d=2.5,h=25);
        translate([0,0,+2.5])cylinder(d=3.2,h=25);
    }
    
    translate([0,0,-he-4]) cylinder(h=he-2.5,d1=25,d2=13); // foot
}



module spoolholder(){
    translate([0,0,-2-1])cylinder(d=12.5,h=12.6);
    translate([0,0,12.6-2-1])sphere(d=12.5);
    translate([0,0,-2-1]) cylinder(d=50,h=1); 
    translate([0,0,-he/2]) cylinder(d2=50,d1=25,h=he/2-3);
    difference(){
        translate([0,0,-he]) cylinder(d1=50,d2=25,h=he/2);
        for(i=[0:90:360]) {
            rotate([0,0,i]) translate([20,0,-he]) cylinder(d=3,h=10);
            rotate([0,0,i]) translate([20,0,-he+3]) cylinder(d=6,h=10);
        }
    }
}

module motor(){
    translate([-6,-5,0])cube([12,10,9.3]);
    cylinder(d=4.2,h=10);
    difference(){
        cylinder(d=3.1,h=20);
        translate([1,-5,10]) cube([10,10,10]);
        }
    translate([-6-12,-5,0])cube([12,10,4.2+5.1]);
    translate([-12,0,0])
        intersection(){
            translate([-6,-5,0]) cube([12,10,21]);
            cylinder(d=12,h=21);
            }
}


module motorspool(){
    difference(){
        union(){
            translate([0,0,-2-1]) cylinder(d=12.5,h=12.6);
            translate([0,0,12.6-3])sphere(d=12.5);
            translate([0,0,-2-1]) cylinder(d=25.5,h=2); 
            for(i=[0:120:360]) {
                    rotate([0,0,i]) translate([12.5/2,-1.5/2,-1]) cube([3,1.5,5]);
            }
            translate([0,0,-14.8]) cylinder(d=11,h=12); 
        }
    
        translate([0,0,-21-4])motor();
    }
    
    difference(){
        hull(){
            translate([-6,0,-20]) cube([24+4,10+4,8],true);
            translate([0,0,-he]) cylinder(d=50,h=1);
        }
        translate([0,0,-21-4])motor();
        for(i=[0:90:360]) {
            rotate([0,0,i+45]) translate([20,0,-he]) cylinder(d=3,h=15);
            rotate([0,0,i+45]) translate([20,0,-he+3]) cylinder(d=6,h=25);
        }
    }
}


module motholder(){
    
    //cylinder(d=6,h=36.6+4);

    difference(){
        translate([-23,-23,0])cube([46,46,15]);
        translate([-20,-20,4])cube([40,40,12]);
        cylinder(d=6,h=10);
    }
}

translate([-80,0,-he]) motholder();

%cube([200,0.3,8]);

spoolholder();
translate([80,0,0]) motorspool();

//!parts(false);
translate([130,10,4]) roller(20,false);