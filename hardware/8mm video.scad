$fn=60;

module picam(){
    color("blue")translate([0,-2.5,-1.0]) import("RPi_Camera_V2.1.stl");
    translate([10.5,0.10,0])cylinder(d=1.7,h=6);
    translate([-10.5,0,0])cylinder(d=1.7,h=6);
    translate([10.5,-12.6,0])cylinder(d=1.7,h=6);
    translate([-10.5,-12.6,0])cylinder(d=1.7,h=6);
    translate([0,0,2])cube([8.7,8.7,8],center=true);
    translate([0,-5,1])cube([12,15,2],center=true);
}

module picamext(){
difference(){    
    union(){
        translate([0,-2,-2.5/2])cube([25,25,2.5],center=true);
        translate([0,0,-40-10])cylinder(d=14+3,h=40+10);
        
        difference(){
        hull(){
        translate([0,0,-40-10-10-2.5]) import("M12_mount_raspi_V2.stl");
            translate([0,0,-50-10+10])cylinder(d1=14+2,d2=22,h=50+10-10);
            }
        translate([-20,-20,-68.5]) cube([40,40,10]);
            }
        }
    translate([10.5,0,-30-2.5])cylinder(d1=1,d2=6,h=30);
    translate([-10.5,0,-30-2.5])cylinder(d1=1,d2=6,h=30);
    translate([10.5,0,-6])cylinder(d=2.1,h=6);
    translate([-10.5,0,-6])cylinder(d=2.1,h=6);
    translate([10.5,-12.6,-6])cylinder(d=2.1,h=6);
    translate([-10.5,-12.6,-6])cylinder(d=2.1,h=6);
    translate([0,0,-40-10-10])cylinder(d=14-1.3,h=40+10+10);
    translate([0,0,-40])cylinder(d1=14-2,d2=14.8,h=2.1);
    translate([0,0,-40+2])cylinder(d=14.8,h=40-2);
    translate([0,15,-38])cylinder(d=20,h=20);
    translate([0,-15,-38])cylinder(d=20,h=20);
}
translate([0,0,-40-10-10-2.5]) import("M12_mount_raspi_V2.stl");
}

!translate([0,0,-22-10]) rotate([0,0,90]) picamext();

module puzzle(){
    w= 8;
    l= 12;
    o = 3;
    t= 0.1;
    z = 1.5;
    difference(){
        union(){
            translate([o,-w/2,0])cube([l-o,w,10]);
            translate([0,-w/2-o,z])cube([o+2*t,w+o*2,10]);
        }
        translate([t+o,-w/2+t,0])cube([l-o-2*t,w-2*t,10]);
        translate([t,-w/2-o+t,z+t])cube([o,(w+o*2)-2*t,10]);
    }
    
}

module bridge(){
    w = 20;
    wi = 9;
    h= 6;
    d = 6;
    hi = 3.5; // thicker than plate
    
    difference(){
        union(){
            translate([-d/2,-w/2,0])cube([d,w,h]);
            cylinder(d=d,h=h+5);
            }
        translate([-d/2,-wi/2,0])cube([d,wi,hi]);
        cylinder(d=3.5,h=20);
    }
}

module strip(){
    // camera side clearance hole
    hull(){
        translate([0,-leadin/2,-0.5])cube([6+1,8+1-leadin,1],true);
        translate([0,0,-17])cube([3+8,3+8,1],true);
    }
    // light side clearance  hole
    leadin = 1;
    
    hull(){
        
        translate([-(6+2)/2,-(8+1.5)/2,0])cube([6+2,8+1.5-leadin,1]);
        translate([0,0,17])cube([18,18,1],true);
    }
    translate([0,0,5]) cube([12.5,12.5,6],true); // light foil
    th = 0.15*2+0.1; // double thickness for splice + some tolerance
    translate([0,0,-th/2]) cube([100,8+0.2,th],true); // the film
}

module panescrew(){
    translate([0,0,0]) cylinder(d=3,h=10);
    translate([0,0,3]) cylinder(d=6,h=50);
    translate([0,0,-12]) cylinder(d=2.6,h=12);
}

module cutpane(){
    //translate([0,0,0.05])cube([100,30,0.1],true);
    l = 25;
    w=7;
    translate([l,w,0]) panescrew();
    translate([l,-w,0]) panescrew();
    translate([-l+5,w,0]) panescrew();
    translate([-l+5,-w,0]) panescrew();
}

module motorscrew(){
    translate([0,0,0])cylinder(d=3,h=10);
    translate([0,0,3])cylinder(d=6,h=40);
}

module motor(clr=false){
    add=clr?0.5:0;
    translate([0,0,-15])cube([40+add,40+add,30],center=true);
    translate([0,0,-30-5])cylinder(d=5,h=3.5);
    cylinder(d=22.5,h=3.3);
    translate([0,0,3])cylinder(d=10.5,h=17.5+1);
    translate([0,0,11+3.5+1.6]) cylinder(d=12.8+add,h=2,center=true); // rubber
    translate([0,0,11-3.5+1.6]) cylinder(d=12.8+add,h=2,center=true); // rubber
    
    translate([31/2,31/2,0])motorscrew();
    translate([31/2,-31/2,0])motorscrew();
    translate([-31/2,31/2,0])motorscrew();
    translate([-31/2,-31/2,0])motorscrew();
}

module bearing(clr=false){
    beardia=clr?16.5:16;
    cylinder(d=beardia,h=10.4,center=true);
    cylinder(d=5,h=40,center=true);
    translate([0,-40/2,0])cube([beardia,40,10.4],center=true);
}


module bodey_up(){
    w = 25.2;  // width of PiCam module
    length = 60;
    runin_dia = 5;
    difference(){
        union(){
            translate([4,0,3/2])cube([length,w,3],center=true); // the upper film guide
            //motor support
            difference(){
                translate([-6,2.6,0])cube([40,10,26]);
                translate([14,1,12.8/2]) rotate([-90,0,0])cylinder(d=18,h=10);
            }
            translate([-6,-w/2,16.6])cube([40,w,10]);
        }
        parts(true);
        translate([-15.5,0,0])puzzle();
        translate([-9,0,1.5]) cylinder(d=4,h=2);
    }
    translate([-9,0,3])bridge();
}

module bodey_low(){
    w = 25.2;  // width of PiCam module
    length = 60;
    runin_dia = 5;
    difference(){
        union(){
            hull(){
                translate([2,0,-21])cube([25,w,1],true); // camera
                translate([4,0,-3/2])cube([length,w,3],center=true); // the lower film guide
            }
            //motor support
            difference(){
                translate([14,1,12.8/2]) translate([-20,6.6-5,0-20])cube([40,10,12]);
                translate([14,1,12.8/2]) rotate([-90,0,0])cylinder(d=18,h=10);
            }
        }
        parts(true);
        rotate([90+45,0,0]) translate([0,0,-10]) cylinder(d=8,h=50);
    }
}


module parts(clr){

translate([0,0,-22]) rotate([0,0,90])picam();
strip();
cutpane();

translate([14,12.6,12.8/2])rotate([90,0,0])motor(clr);
translate([14,12.6,12.8/2-0.5])rotate([90,0,0])motor(clr);
translate([14,12.6,12.8/2+0.5])rotate([90,0,0])motor(clr);
//translate([14,12.6,12.8/2-1.0])rotate([90,0,0])motor(clr);
translate([14,0,-16/2-0.3])rotate([90,0,0])bearing(clr);
    
}

//translate([-35,0,20/2]) rotate([90,0,0]) roller(20,false,true);
//translate([-135,0,20/2]) rotate([90,0,0]) roller(20,false,false);
bodey_up();
translate([0,0,-10])bodey_low();


