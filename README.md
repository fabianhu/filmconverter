Apparatus for digitizing 8mm film

Ingredients:
* Raspberry Pi 
* Raspberry Pi Camera V2 
* 16mm lens with 12mm thread
* Arduino Uno with CNC shield
* a random stepper motor
* ADUM1201 isolator for the serial connection
* geared motor
* ULN2803 driver
* 7W LED spot
* step-up and step-down DCDC-converters
* some old movies
