#!/usr/bin/env python3

# convert with ffmpeg -framerate 18 -i image%d.png -c:v libx264 -r 18 -pix_fmt yuv420p out.mp4
# ffmpeg -framerate 9 -i image%d.png -c:v libx264 -r 9 -pix_fmt yuv420p out_9fps.mp4
# ffmpeg -framerate 18 -i image%d.png -c:v libx264 -r 18 -pix_fmt yuv420p out_18fps.mp4
# ffmpeg -framerate 18 -i image%d.png -c:v libx264 -r 18 -pix_fmt yuv420p -vf hflip Zoo_N8_18fps.mp4

import time
import picamerax as picamera
import numpy as np
import cv2
import jsonplus as json
from gpiozero import LED, Button

capturedirectory = "../../p/capture/"

led = LED(26)
led.on()

camera = picamera.PiCamera()
camera.resolution = (3280, 2464)  # (320, 240)
camera.framerate = 5
# camera.vflip = True this is bad during calibration!
camera.iso = 100
camera.awb_mode = 'auto'
camera.shutter_speed = 360  # fixme find automatically

# ctable = camera._get_lens_shading_table() # original table can not be read.

ctable = np.full(camera._lens_shading_table_shape(), 32, dtype=np.ubyte)  # the table
_, th, tw = camera._lens_shading_table_shape()
print(f"shading table shape {camera._lens_shading_table_shape()}")

image = np.empty((2464 * 3296 * 3,), dtype=np.uint8)

ctable_f = (np.zeros((th * tw * 3,), dtype=np.float)).reshape((th, tw, 3))  # empty correction
ctable_f_raw = (np.zeros((th * tw * 3,), dtype=np.float)).reshape((th, tw, 3))  # empty correction

camera.lens_shading_table = ctable  # set table, not None, which uses the default table.
time.sleep(1)

for i in range(10):
    camera.capture(image, 'bgr')
    image = image.reshape((2464, 3296, 3))
    # cv2.imwrite(capturedirectory + "uncorrected" + str(i) + ".png", image)

    roi_left = 600
    roi_right = 2900
    roi_top = 60
    roi_btn = 2120

    img_roi = image[roi_top:roi_btn, roi_left:roi_right]

    emp = np.full((2464 * 3296 * 3,), 12, dtype=np.uint8)  # the perfect empty image
    emp = emp.reshape((2464, 3296, 3))

    emp = cv2.copyMakeBorder(img_roi, roi_top, 2464 - roi_btn, roi_left, 3296 - roi_right,
                             borderType=cv2.BORDER_REPLICATE)

    average = img_roi.mean(axis=0).mean(axis=0)
    roi_maxv = img_roi.max()
    av = (average[0] + average[1] + average[2]) / 3
    print(f"average: {average} - {av}")

    emp[roi_btn - 15:, roi_right - 15:] = emp[roi_btn - 25:roi_btn - 15, roi_right - 25:roi_right - 15].mean(
        axis=0).mean(axis=0)  # fill bottom right
    # cv2.imwrite(capturedirectory + "roi" + str(i) + ".png", emp)

    ime = cv2.resize(emp, (tw, th))
    ima = np.array(ime, dtype=np.float)

    ctable_f_raw = av / ima
    ctable_f += ctable_f_raw
    ctable_f -= ctable_f.min()
    ctable_f += 1
    ctable_f[33:, 45:] = ctable_f[33 - 2, 45 - 2]  # fill bottom right corner with useful values.

    print(f"iteration {i}")

    cv2.imwrite(capturedirectory + "corrTable" + str(i) + ".png", ctable_f * 32)

    b, g, r = cv2.split(ctable_f * 32)
    ctable = np.array((r, g, g, b), dtype=np.uint8)
    camera.lens_shading_table = ctable

    time.sleep(0.5)

ctable_f = cv2.blur(ctable_f, (3, 3))  # blur once only

cv2.imwrite(capturedirectory + "corrTable_final.png", ctable_f * 32)
b, g, r = cv2.split(ctable_f * 32)
ctable = np.array((r, g, g, b), dtype=np.uint8)

awb_data = (camera.awb_gains, ctable.tolist(), ctable.shape)

with open("whitebalance.json", "w") as f:
    j = json.dumps(awb_data, f)
    f.write(j)
    f.close()

camera.awb_mode = 'off'
camera.awb_gains = awb_data[0]
time.sleep(2)
image = np.empty((2464 * 3296 * 3,), dtype=np.uint8)

camera.capture(image, 'bgr')

image = image.reshape((2464, 3296, 3))

cv2.imwrite(capturedirectory + "corrected.png", image)
