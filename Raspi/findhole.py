#!/usr/bin/python3
import cv2
import json
import numpy as np
import imutils

imnum = 0

templateN = cv2.imread("N8hole.png")
templateS = cv2.imread("S8hole.png")
templateN_gray = cv2.cvtColor(templateN, cv2.COLOR_BGR2GRAY)
templateS_gray = cv2.cvtColor(templateS, cv2.COLOR_BGR2GRAY)


def findhole(frame, x, w, fmt):
    global imnum

    th = tw = 0
    yoffset = 0
    res = None
    fh, fw = frame.shape[1::-1]
    
    if fmt == 'N':
        yoffset = 800
        crop = frame[yoffset:fh, x:x + w].copy()
        image_gray = cv2.cvtColor(crop, cv2.COLOR_BGR2GRAY)
        res = cv2.matchTemplate(image_gray, templateN_gray, cv2.TM_CCOEFF_NORMED)
        tw, th = templateN.shape[1::-1]
    elif fmt == 'S':
        yoffset = 0
        crop = frame[0:fh, x:x + w].copy()
        image_gray = cv2.cvtColor(crop, cv2.COLOR_BGR2GRAY)
        res = cv2.matchTemplate(image_gray, templateS_gray, cv2.TM_CCOEFF_NORMED)
        tw, th = templateS.shape[1::-1]
    else:
        print("call error")
        exit(1)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    # cv2.imwrite("/tmp/crp.png", image_gray)
    xp, yp = max_loc
    xp += tw      # right corner
    yp += th / 2 + yoffset  # center in hole

    '''
    cv2.circle(crop, (int(xp),int(yp)), 7, (0, 0, 255), -1)
    #cv2.imshow("Crop",crop)
    
    cv2.circle(frame, (int(xp+x),int(yp)), 7, (0, 0, 255), -1)
    startpoint = (int(xp+x-1150),int(yp-850))
    endpoint = (int(xp+x),int(yp))
    cv2.rectangle(frame, startpoint, endpoint, 255, 2)
    cv2.imshow("Frame",frame) 
      
    imnum += 1
    fn = "p/capture/ana" + str(imnum) + ".png"
    #cv2.imshow("Image",image)
    #cv2.imwrite(fn, res) 
    '''

    return xp + x, yp  # correct x for the cropped offset


def main():
    """ Main entry point of the app """
    f = cv2.imread("imageS8.png")
    # cv2.imshow("Result", f)

    xoff, yoff = findhole(f, 300, 200, 'S')
    print(xoff, yoff)
    w0, h0 = f.shape[1::-1]
    w = 1200  # 1280 x 960 ? finally 1280x800
    h = 900
    x = 30
    y = int((h0 - h) / 2 + yoff)
    print(h0, w0, h, yoff, y)

    if abs(yoff) > (h0 - h) / 2:
        print("huge offset")

    crop = f[y:y + h, x:x + w].copy()

    cv2.imshow("Result", crop)

    cv2.waitKey(0)


if __name__ == "__main__":
    """ This is executed when run from the command line """
    main()
