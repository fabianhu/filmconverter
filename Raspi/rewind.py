#!/usr/bin/env python3

import time
import serial
import io
import picamera
import os
# sudo pip3 uninstall picamera -y
# sudo pip3 install "picamera[array]"==1.10

from picamera.array import PiRGBArray
import numpy as np
import cv2
from findhole import findhole
import jsonplus as json
from gpiozero import LED, Button

led = LED(26)
mot1 = LED(13)
mot2 = LED(6)
motf = LED(5)

print("start")
motf.on()
time.sleep(120*2)
print("stop")
motf.off()
