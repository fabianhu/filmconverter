#!/usr/bin/env python3

# pip3 install 'picamerax'
# from https://github.com/labthings/picamerax
# pip3 install 'opencv-python'

# convert with ffmpeg -framerate 18 -i image%d.png -c:v libx264 -r 18 -pix_fmt yuv420p out.mp4
# ffmpeg -framerate 9 -i image%d.png -c:v libx264 -r 9 -pix_fmt yuv420p out_9fps.mp4
# ffmpeg -framerate 18 -i image%d.png -c:v libx264 -r 18 -pix_fmt yuv420p out_18fps.mp4
# ffmpeg -framerate 18 -i image%d.png -c:v libx264 -r 18 -pix_fmt yuv420p Zoo_N8_18fps.mp4

# rsync -auv /mnt/sda/capture/ /home/pi/p/capture
# mv /mnt/sda/*.png /home/pi/p/capture/

# concatenate videos:
# $ cat mylist.txt  
# file '/path/to/file1'
# file '/path/to/file2'
# file '/path/to/file3'  
# $ ffmpeg -f concat -safe 0 -i mylist.txt -c copy output.mp4

import time
import serial
import picamerax as picamera
import os
import threading

from cv2 import VideoWriter
from picamerax.array import PiRGBArray
import numpy as np
import cv2
from findhole import findhole
import jsonplus as json
from gpiozero import LED, Button
import sys

shutterspeed = 0  # 0 for auto
filmtype = None
isHDR = False
isFocusTest = False

capturedirectory = "../../p/capture/"
capturedirectoryalt = "/mnt/sda/"
safefilename = "image"

dirlist = os.listdir(capturedirectoryalt)
for x in dirlist:
    if len(x) >= 4 and x[-4:] == ".png":
        print(f"There are {len(dirlist)} files in emergency storage {capturedirectoryalt}.")
        print("Now cleaning up")
        os.system("./cleanup.sh")
        print("finished with cleaning - exiting now")
        exit(1)

if len(sys.argv) >= 2:
    if sys.argv[1] == 'N':
        filmtype = 'N'
    if sys.argv[1] == 'S':
        filmtype = 'S'

if not filmtype:
    print(f"usage: {__file__} N|S [<shutter speed>|HDR [D]]")
    print("N = normal 8")
    print("S = super 8")
    print(f"<shutter speed> in us, defaults to \"auto\"")
    print("D produces extra debug output images")
    print("examples:")
    print(f"{__file__} S  # Super8 capture")
    print(f"{__file__} S 3000 # Super8 capture with 3ms exposure time")
    print(f"{__file__} N HDR  # capture Normal8 in HDR (use with dark film)")
    exit(1)

if len(sys.argv) >= 3:
    if sys.argv[2] == 'HDR':
        isHDR = True
        print("HDR mode activated")
    else:
        shutterspeed = int(sys.argv[2])
        print(f"shutter speed set to {shutterspeed}")

debug = False
isVideoOut = False

if len(sys.argv) == 4:
    if sys.argv[3] == 'D':
        debug = True
        isVideoOut = True

led = LED(26)
mot1 = LED(13)
mot2 = LED(6)
motf = LED(5)

if filmtype == 'N':
    holebegin = 650
    holewidth = 650
    ysetpoint = 1725  # at the bottom
    defaultstep = 303
    cropwidth = 1650  # 1280 x 960 ? finally 1280x800
    cropheight = 1230
    cropdiv = 1  # 1 = center of the hole is at the bottom of the frame
elif filmtype == 'S':
    holebegin = 550
    holewidth = 335
    ysetpoint = 1230  # half of height
    defaultstep = 335
    cropwidth = 2000  # 1280 x 960 ? finally 1280x800
    cropheight = 1350
    cropdiv = 2  # 2 = center of the hole is at the center of the frame
else:
    print("filmtype errpr")
    holebegin = 1450
    holewidth = 300
    ysetpoint = 540  # half of 1080
    defaultstep = 335
    cropwidth = 1350  # 1280 x 960 ? finally 1280x800
    cropheight = 1000
    cropdiv = 2  # 2 = center of the hole is at the center of the frame
    exit(1)

stepdiv = (cropheight * 0.95) / defaultstep
# print("stepdiv" + str(stepdiv))

with open('whitebalance.json', 'r') as f:
    whitebalance, ctable, cshape = json.loads(f.read())

if whitebalance is None or ctable is None:
    print("generate whitebalance reference without film first")
    exit(1)

ctable = np.array(ctable, dtype=np.uint8)
ctable = np.reshape(ctable, cshape)
print("AWB loaded")

if isVideoOut:
    out: VideoWriter = cv2.VideoWriter(capturedirectory + 'video.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 18,
                                       (cropwidth, cropheight))

ms = time.time() * 1000.0

fsckcnt = 0


def safeimage(_num, _image):
    global fsckcnt
    numthreads = len(threading.enumerate()) - 1
    if numthreads == 1:
        print("already 1 thread running")
    elif numthreads >= 2:
        print(f"already {numthreads} threads running")

    if isFocusTest:
        fna = "/tmp/" + "test.png" + str(_num) + ".png"
        cv2.imwrite(fna, _image)
        file_stats = os.stat(fna)
        print(f'File size {file_stats.st_size} bytes')
        return
    elif numthreads < 3:
        fna = capturedirectory + safefilename + str(_num) + ".png"
        mythread = threading.Thread(target=cv2.imwrite, args=(fna, _image))
        mythread.setDaemon(True)
        mythread.start()
    else:
        fna = capturedirectoryalt + safefilename + str(_num) + ".png"
        cv2.imwrite(fna, _image)  # no threading as we are slow anyway
        print(f"using emergency storage {fna}")
        fsckcnt += 1


def prnt(args):
    global ms
    tm = time.time() * 1000.0
    dif = tm - ms
    ms = tm
    print(round(dif), args)


def drive(steps):
    ser.write(("r" + str(int(-steps))).encode())


ser = serial.Serial(
    port='/dev/ttyS0',  # Replace ttyS0 with ttyAM0 for Pi1,Pi2,Pi0
    baudrate=115200,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=1
)
print("wait for serial")
time.sleep(2)
ser.write('1'.encode())
# ser.write('AX10000'.encode())
images = []

led.on()

# initialize the camera and grab a reference to the raw camera capture
camera = picamera.PiCamera()
# (1920, 1088) results in a heavily cropped view.. magically this matches the FOV on film
camera.resolution = (3280, 2464)  # (1920, 1088)
camera.framerate = 10  # max 15 according doc
# camera.zoom = (0.2, 0.15, 0.6, 0.75)
camera.iso = 100

if shutterspeed == 0:
    camera.shutter_speed = 0  # set to 0 for auto
    camera.meter_mode = 'spot'  # 'average'
    camera.exposure_mode = 'auto'
else:
    camera.shutter_speed = shutterspeed  # set to 0 for auto
# camera.exposure_mode = 'off' 'auto' # should do this only after some exposures. -> black otherwise

camera.vflip = True
camera.awb_mode = 'off'
camera.awb_gains = whitebalance
if isHDR:
    camera.drc_strength = 'high'  # test for a very dark film

# camera.awb_mode = 'auto'  # according doc, this does AWB recalculation. So we turn this on here. .. or not

camera.lens_shading_table = ctable

# grab an image from the camera
prnt("capture " + str(camera.resolution) + " start")
image = np.empty((camera.resolution[1], camera.resolution[0], 3), dtype=np.uint8)

rawCapture = PiRGBArray(camera)  # , size=camera.resolution)

j = 1
k = 1
cr = 1
uncorrected = True

stepped = 0

# find last saved file to enable continuation after stop
i = 1
while os.path.exists(capturedirectory + safefilename + str(i) + ".png"):
    i += 1

print("starting from " + str(i))
noframe = 0

time.sleep(2)
# video not for full res.
for frame in camera.capture_continuous(rawCapture, format='bgr', use_video_port=False, burst=False):
    image = frame.array
    rawCapture.truncate(0)  # throw away stuff we have already seen or missed

    xoff, yoff = findhole(image, holebegin, holewidth, filmtype)

    if noframe > 50:
        print("No valid frame for 50 tries")
        mot1.off()
        mot2.off()
        led.off()
        ser.write('0'.encode())
        break

    if debug:
        dimage = image.copy()
        w0, h0 = dimage.shape[1::-1]
        cv2.rectangle(dimage, (holebegin, 0), (holebegin + holewidth, h0), (0, 255, 0), 2)  # hole search area
        cv2.circle(dimage, (int(xoff), int(yoff)), 7, (0, 0, 255), -1)

        fn = capturedirectory + "debug" + str(k) + ".png"
        k += 1
        cv2.imwrite(fn, dimage)

    if yoff is None or abs(yoff - ysetpoint) > 600:
        drive(50)  # 10 makes about 30 offset
        stepped += 10
        time.sleep(0.1)
        uncorrected = False
        noframe += 1
        '''fn = capturedirectory + "eng" + str(j) + ".png"
        j += 1
        cv2.imwrite(fn, image)'''
        continue

    if abs(yoff - ysetpoint) > 25 and not isFocusTest:
        diff = (yoff - ysetpoint) / stepdiv
        print("diff", yoff - ysetpoint)
        drive(diff)  # we should never drive back
        stepped += diff
        time.sleep(0.2)
        uncorrected = False
        noframe += 1
        continue

    if uncorrected:
        if yoff - ysetpoint < -5:
            defaultstep -= 1
        elif yoff - ysetpoint > 5:
            defaultstep += 1

    drive(defaultstep)
    mot1.on()
    mot2.on()
    drivetime = time.time() * 1000.0
    prnt(
        "click " + str(i)
        # + " steps " + str(round(stepped))
        # + " offs " + str(round(yoff - ysetpoint))
        + " " + str(round(camera.exposure_speed / 1000, 1)) + "ms"
    )

    cropx = int(xoff) - 30
    cropy = int(yoff - cropheight / cropdiv) + 30

    # handle edge cases
    if cropx <= 0:
        cropx = 0
    if cropy <= 0:
        cropy = 0

    if debug:
        dimage = image.copy()
        w0, h0 = dimage.shape[1::-1]
        cv2.rectangle(dimage, (holebegin, 0), (holebegin + holewidth, h0), (0, 255, 0), 2)  # hole search area
        cv2.circle(dimage, (int(xoff), int(yoff)), 7, (0, 0, 255), -1)
        startpoint = (int(cropx), int(cropy))
        endpoint = (int(cropx + cropwidth), int(cropy + cropheight))
        cv2.rectangle(dimage, startpoint, endpoint, 255, 2)

        fn = capturedirectory + "debug_" + str(k) + ".png"
        k += 1
        cv2.imwrite(fn, dimage)

    image_cropped = image[cropy:cropy + cropheight, cropx:cropx + cropwidth]
    if isVideoOut:
        out.write(image_cropped)

    stepped = defaultstep

    # cv2.imwrite(fn, image_cropped)  # sometimes writing takes forever - restart of the pi helps.
    safeimage(i, image_cropped)
    noframe = 0
    image_cropped = None

    while time.time() * 1000.0 - drivetime < 400:
        time.sleep(0.1)

    i += 1

    uncorrected = True
    mot1.off()
    mot2.off()

prnt("capture end")

print(f"We fscked up {fsckcnt} times.")
if fsckcnt > 0:
    print("Now cleaning up")
    os.system("./cleanup.sh")
    print("finished with cleaning - exiting now")
