#!/usr/bin/env python3
import time
import serial
import io

ser = serial.Serial(
        port='/dev/ttyS0', #Replace ttyS0 with ttyAM0 for Pi1,Pi2,Pi0
        baudrate = 115200,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1
)

ser.write('1'.encode())
ser.write('R10000'.encode())
time.sleep(5)
ser.write('0'.encode())

'''
while 1:
    x=ser.readline()
    print(x)
    time.sleep(0.5)
'''

