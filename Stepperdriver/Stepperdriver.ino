// steppertest
// -*- mode: C++ -*-

#include <AccelStepper.h>

// X and Y are swapped in HW due to mechanical constraints
AccelStepper stepperX(AccelStepper::DRIVER, 2, 5); // mode, step, dir
AccelStepper stepperY(AccelStepper::DRIVER, 3, 6); // mode, step, dir


#define PIN_enable 8


int g_xpos = 0;
bool g_zeroed = false;

void setup() {
  Serial.setTimeout(50);
  Serial.begin(115200);

  pinMode(PIN_enable, OUTPUT); // enable steppers (all at once) low-active

  digitalWrite(PIN_enable, HIGH);

  // Configure each stepper
  stepperX.setMaxSpeed(100000);
  stepperX.setAcceleration(10000);
  stepperX.setMinPulseWidth(20);

  stepperY.setMaxSpeed(MAXSPEED_Y);
  stepperY.setAcceleration(12000);
  stepperY.setMinPulseWidth(20);

  Serial.println("Move Axis: X200");
  Serial.println("Change Accel for Axis: AX1000");
  Serial.println("Change Speed for Axis: SX1000");
  Serial.println("Motors Enable: 1");
  Serial.println("Motors Disable: 0");

  //zero();
}

void loop() {
  stepperX.run();
  stepperY.run();
  checkSerial();
}





// ************* functional stuff ********************

void readEmpty()
{
  while (Serial.available())
    Serial.read();
}


void checkSerial()
{
  char c;
  int x;
  if (Serial.available())
  {
    //statements
    c = Serial.read();

    switch (c)
    {

      case 't':
      case 'T':
        //test();
        break;

      case 'X':
      case 'x':
        x = Serial.parseInt();
        stepperX.moveTo(x * SUBSTEPS_X);
        Serial.print("X->");
        Serial.print(x, DEC);
        Serial.print("\n");
        break;

      case 'R':
      case 'r':
        x = Serial.parseInt();
        stepperX.move(x * SUBSTEPS_X);
        Serial.print("Run X->");
        Serial.print(x, DEC);
        Serial.print("\n");
        break;


      case 'Y':
      case 'y':
        x = Serial.parseInt();
        stepperY.moveTo(x );
        Serial.print("Y->");
        Serial.print(x, DEC);
        Serial.print("\n");
        break;

      case 'S':
        while (!Serial.available());
        c = Serial.read();
        switch (c)
        {
          case 'X':
            x = Serial.parseInt();
            stepperX.setMaxSpeed(x);
            break;

          case 'Y':
            x = Serial.parseInt();
            stepperY.setMaxSpeed(x);
            break;

          default:
            readEmpty();
            x = 0;
            ;
        }
        Serial.print("Speed ");
        Serial.print(c);
        Serial.print("=");
        Serial.print(x, DEC);
        Serial.print("\n");
        break;

      case 'A':
        while (!Serial.available());
        c = Serial.read();
        switch (c)
        {
          case 'X':
            x = Serial.parseInt();
            stepperX.setAcceleration(x);
            break;

          case 'Y':
            x = Serial.parseInt();
            stepperY.setAcceleration(x);
            break;

          default:
            readEmpty();
            x = 0;
            ;
        }
        Serial.print("Accel ");
        Serial.print(c);
        Serial.print("=");
        Serial.print(x, DEC);
        Serial.print("\n");
        break;
      //stepperY.setAcceleration(50000);

      case '1':
        digitalWrite(PIN_enable, LOW); // enable steppers (all at once) low-active
        Serial.print("Steppers ON\n");
        break;

      case '0':
        digitalWrite(PIN_enable, HIGH); // enable steppers (all at once) low-active
        Serial.print("Steppers OFF\n");
        break;

      default:
        readEmpty();
        ;
    }
    //Serial.write(c);
  }
}
